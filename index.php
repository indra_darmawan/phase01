<?php 
    date_default_timezone_set("Asia/Makassar");
    
    include "helper.php";
    $helper = new Helper();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="Island Media Management - Indra Darmawan" />
        <meta name="author" content="Indra Darmawan" />
        <title>Island Media Management - Indra Darmawan</title>
        <link rel="icon" type="image/png" href="assets/icon.png" />
        <link href="css/bootstrap.min.css" rel="stylesheet" />
        <link href="css/style.css?ver=<?=$helper->version()?>" rel="stylesheet" />
    </head>
    <body>
        
        <div class="container">
            <div class="row">
                <div class="col-sm-12 mb-4 mt-4">
                    <div class="imm-advanced-link mb-1">[<a href="#" class="advanced-link" id="advancedLink" data-flag="1">show datetime</a>]</div>
                    <div class="card imm-base">
                        <div class="card-body text-center imm-inner">
                            <p class="mt-3">Today is <span class="dayname-today"><?= $helper->today() ?></span></p>
                            <p class="mt-2 mb-3 imm-showdatetime hidden">
                                <span id="immDate"></span> <span id="immTime"></span>
                            </p>
                            <div class="btn-days">
                                <?php 
                                    /*looping for days*/
                                    foreach ($helper->days() as $key => $value) {
                                        echo "<button class='btn btn-day ".$helper->istoday($key)."' data-num='".$key."'>".$value."</button>";
                                    }
                                ?>
                            </div>
                            <p class="mt-3" id="selectedDay">Click on one of the day names above.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

		<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        <script src="js/scripts.js?ver=<?=$helper->version()?>"></script>
    </body>
</html>