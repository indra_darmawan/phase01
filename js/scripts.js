$( document ).ready(function() {

	$(".btn-day").click(function() {

		// remove all class selected, if exists
		$(".btn-day").removeClass("selected"); 

		// add class selected to button clicked
		$(this).addClass("selected"); 

		// set value to element selectedDay
		$("#selectedDay").html( "Selected day is <span class='dayname-selected'>"+$(this).text()+'</span>' );
	});

	$(".advanced-link").click(function() {
		var flag = $(this).data('flag');

		if(flag == '1') {
			$(this).html('hide datetime');
			$(this).data('flag', '0');
			$(".imm-showdatetime").fadeIn("slow", function() {
			    $(this).removeClass("hidden");
			});
		} else if(flag == '0') {
			$(this).html('show datetime');
			$(this).data('flag', '1');
			$(".imm-showdatetime").fadeOut("slow", function() {
			    $(this).addClass("hidden");
			});
		}

	});

	var immDate = $('#immDate');
	var immTime = $('#immTime');

	function timeclock(){
	  var date = new Date();
	  
	  var meridiem = date.getHours() < 12 ? 'AM' : 'PM';
	  
	  var hours = ( date.getHours() == 0 ? 12 : ( date.getHours() > 12 ? date.getHours() - 12 : date.getHours() ) );
	  hours = (hours < 10 ? '0'+hours : hours);
	  
	  var minutes = ( date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes() );
	  
	  var seconds = ( date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds() );
	  
	  var month = date.getMonth() + 1; // add extra 1, because of months start from 0
	  var day = ( date.getDate() < 10 ? '0'+date.getDate() : date.getDate() );
	  var year = date.getFullYear();
	  
	  var dateString = day + ' ' + date.toLocaleString('default', { month: 'long' }) + ' ' + year;
	  var timeString = hours + ':' + minutes + ':' + seconds + ' ' + meridiem;
	  
	  immDate.html(dateString);
	  immTime.html(timeString);
	} 

	timeclock();
	window.setInterval(timeclock, 1000);


});