<?php 
	/*** collection of functions ***/
	/* 
	/*
	/****** by Indra Darmawan ******/


	class Helper {
		
		function days( $selected = "" ) {

			$days = array(
				'1' => 'Monday',
				'2' => 'Tuesday',
				'3' => 'Wednesday',
				'4' => 'Thursday',
				'5' => 'Friday',
				'6' => 'Saturday',
				'0' => 'Sunday'
			);
			
			if($selected == "") {
				return $days;
			} else {
				return $days[$selected];
			}

		}

		function today() {
			$numtoday = date("N");
			return self::days($numtoday);
		}

		function istoday($num) {
			return ( date("N") == $num ? 'today' : '');
		}

		function version() {
			return "1.1";
		}

		
	}
?>